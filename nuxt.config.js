export default {
	// Global page headers (https://go.nuxtjs.dev/config-head)
	head: {
		title: "VoteLog",
		meta: [
			{ charset: "utf-8" },
			{
				name: "viewport",
				content: "width=device-width, initial-scale=1",
			},
			{ hid: "description", name: "description", content: "" },
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/icon.png" }],
	},

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css: ["~/assets/app.sass", "@fortawesome/fontawesome-svg-core/styles.css"],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins: ["~/plugins/i18n", "~/plugins/nuxt-client-init.client.js", "~/plugins/fontawesome.js"],

	// Auto import components (https://go.nuxtjs.dev/config-components)
	components: ["~/components", "~/components/nav/", "~/components/controls/"],

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules: ["@nuxtjs/color-mode", "@nuxtjs/tailwindcss"],

	colorMode: {
		classSuffix: "",
	},
	// Modules (https://go.nuxtjs.dev/config-modules)
	modules: ["@nuxtjs/axios", "@nuxtjs/i18n", "cookie-universal-nuxt", "@nuxtjs/markdownit"],

	axios: {
		baseURL: `https://api.votelog.ch`,
	},

	i18n: {
		locales: [
			{
				code: "de",
				name: "Deutsch",
				file: "de.js",
				iso: "de-CH",
				dir: "ltr",
			},
			// {
			// 	code: "en",
			// 	name: "English",
			// 	file: "en.js",
			// 	iso: "en",
			// 	dir: "ltr",
			// },
			// {
			// 	code: "fr",
			// 	name: "Français",
			// 	file: "fr.js",
			// 	iso: "fr-CH",
			// 	dir: "ltr",
			// },
			// {
			// 	code: "it",
			// 	name: "Italiano",
			// 	file: "it.js",
			// 	iso: "it-CH",
			// 	dir: "ltr",
			// },
		],
		defaultLocale: "de",
		strategy: "prefix",
		lazy: true,
		langDir: "lang/",
	},

	// See https://github.com/markdown-it/markdown-it
	markdownit: {
		html: true,
		xhtmlOut: true,
		runtime: true,
		preset: "default",
		linkify: false,
		breaks: true,
		typographer: true,
		quotes: ["«", "»", "‹", "›"],
		use: ["markdown-it-texmath"],
	},

	env: {
		content: {
			config: {
				baseURL: `https://gitlab.com/api/v4/projects/33194923/repository/files`,
				headers: {
					Authorization: null,
				},
			},
		},
	},

	// server: {
	// 	host: "0.0.0.0",
	// 	port: 3000,
	// },

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		extractCSS: true,
		// source-map for production debugging; TODO: remove for beta-launch
		extend(config, {isClient}) {
			if (isClient) {
				config.devtool = 'source-map'
			}
		}
	},
};
