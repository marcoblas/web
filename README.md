# votelog-front

[![Health](https://status.votelog.ch/api/v1/endpoints/frontend_prototype/health/badge.svg)](https://status.votelog.ch/endpoints/frontend_prototype)
[![Uptime (24h)](https://status.votelog.ch/api/v1/endpoints/frontend_prototype/uptimes/24h/badge.svg)](https://status.votelog.ch/endpoints/frontend_prototype)
[![Response time (24h)](https://status.votelog.ch/api/v1/endpoints/frontend_prototype/response-times/24h/badge.svg)](https://status.votelog.ch/endpoints/frontend_prototype)

Frontend application of [VoteLog](https://votelog.ch/) built with [Vue](https://vuejs.org/) and [Nuxt](https://nuxtjs.org/).

## Sponsors

[<img alt="Prototype Fund Switzerland" src="https://opendata.ch/wordpress/files/2021/04/PTF-Logo-169.png" width="200"/>](https://prototypefund.opendata.ch/)
[<img alt="OpenData.ch" src="https://prototypefund.opendata.ch/files/2020/02/opendata-logo-schwarz-weiss.jpg" width="238"/>](https://opendata.ch/)
[<img alt="Stiftung Mercator Schweiz" src="https://prototypefund.opendata.ch/files/2022/01/Mercator_Logo_RGB.jpg" width="242"/>](https://www.stiftung-mercator.ch/)

### Hosting

[<img alt="Vercel" src="https://gitlab.com/votelog/web/-/raw/main/static/powered-by-vercel.svg" width="200"/>](https://vercel.com/?utm_source=VoteLog&utm_campaign=oss)

## Licence

This project is licensed under the [GNU Affero General Public License v3.0 or later](https://gitlab.com/votelog/web/-/blob/main/LICENSE) (SPDX identifier [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html)). This allows the (Swiss) public to assess, question and evaluate the code underlying the VoteLog platform. It also enables projects and organizations in other countries working for more transparency and accountability to use VoteLog and build upon it.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Status and Sitemap

- [ ] index
    - [x] basic info
    - [ ] new votings
    - [ ] new organisations
- [ ] organisations-index
    - [ ] Topics Menu
    - [x] alphabetical
    - [ ] search
- [ ] organisation-slug
    - [x] ratings
        - [x] parlamentarian
        - [x] parties
        - [x] factions
    - [x] preferences
    - [ ] information
- [x] parlamentarian-index
    - [x] Filter
- [x] parlamentarian-slug
    - [x] info
    - [x] organisations ranking
- [x] mission
  - [x] mission-slug
- [x] method
- [x] privacy
- [x] imprint
- [x] login/logout
- [ ] registration
- [ ] account
    - [ ] overview
        - [ ] new preferences
    - [x] rate-index
        - [x] search
        - [x] results
            - [x] rate-slug
            - [x] votes
    - [x] preferences
        - [x] change/delete preferences
    - [ ] settings user
    - [ ] settings organisations
      - [ ] settings organisation users
- [ ] admin
    - [ ] edit organisations
    - [ ] add organisation and first user
    - [ ] add/remove roles
    
## Checklist Accessibility

-   ### Global & Content
	-   [x] lang attribute
	-   [ ] html validated (https://validator.w3.org/nu/)
	-   [ ] unique page titles
	-   [ ] linear content flow (tab focus order)
-   ### Images
	-   [ ] img have alt tags
	-   [ ] text alternative for charts, graphs & maps
	-   [ ] alt includes text displayed in the image
-   ### Headings
	-   [ ] headings in logical sequence
	-   [ ] headings are not skipped
-   ### Controls
	-   [ ] <a> tags & "href" are used for links
	-   [x] links are recognizable as such
	-   [x] controls have visible :focus
	-   [ ] links opening new tabs are declared as such
-   ### Forms
	-   [ ] all inputs are associated with a corresponding label
	-   [ ] input error messages assiociate clearly with the corresponding input
-   ### Appearance
	-   [x] website works in specialized browser modes (high contrast, inverted)
	-   [ ] website is legible up to 200% text size
		-   (needs scrollable or compact sidebar if scrolled in too much)
	-   [ ] color is not the only way information is conveyed
	-   [x] animations are subtle and don't flash
-   ### Color Contrast
	-   [x] text is properly contrasted (regular: 4.5:1, large 3:1)
		-   (dark mode needs different blue color)
	-   [x] icons are properly contrasted (3:1)
	-   [x] borders of input elements are properly contrasted (3:1)
